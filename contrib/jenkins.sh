#!/bin/bash

TOPDIR=`pwd`

set -e

publish="$1"


# make sure the submodules are upt-o-date
git submodule init
git submodule update

# build the library once so that the irq2nvic script is running, generating headers
(cd libopencm3 && make)

echo "=============== libopencm3 START  =============="
cd "$TOPDIR"
make $PARALLEL_MAKE lib
echo "=============== libopencm3 DONE =============="

PROJECTS="relay-driver rfdsatt "

cd $TOPDIR/projects
for proj in $PROJECTS; do
	echo "=============== $proj START  =============="
	cd "$TOPDIR/projects/$proj"
	make clean
	make
	echo "=============== $proj DONE =============="
done

if [ "x$publish" != "x--publish" ]; then
	exit 0
fi

cat > "/tmp/known_hosts" <<EOF
[ftp.osmocom.org]:48 ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDDgQ9HntlpWNmh953a2Gc8NysKE4orOatVT1wQkyzhARnfYUerRuwyNr1GqMyBKdSI9amYVBXJIOUFcpV81niA7zQRUs66bpIMkE9/rHxBd81SkorEPOIS84W4vm3SZtuNqa+fADcqe88Hcb0ZdTzjKILuwi19gzrQyME2knHY71EOETe9Yow5RD2hTIpB5ecNxI0LUKDq+Ii8HfBvndPBIr0BWYDugckQ3Bocf+yn/tn2/GZieFEyFpBGF/MnLbAAfUKIdeyFRX7ufaiWWz5yKAfEhtziqdAGZaXNaLG6gkpy3EixOAy6ZXuTAk3b3Y0FUmDjhOHllbPmTOcKMry9
[ftp.osmocom.org]:48 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBPdWn1kEousXuKsZ+qJEZTt/NSeASxCrUfNDW3LWtH+d8Ust7ZuKp/vuyG+5pe5pwpPOgFu7TjN+0lVjYJVXH54=
[ftp.osmocom.org]:48 ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK8iivY70EiR5NiGChV39gRLjNpC8lvu1ZdHtdMw2zuX
EOF

SSH_COMMAND="ssh -o 'UserKnownHostsFile=/tmp/known_hosts' -p 48"

for proj in $PROJECTS; do
	echo "=============== $proj UPLOAD =============="
	cd "$TOPDIR/projects/$proj"

	LATEST_BIN="$(ls -1 -t "$proj"-*.bin | head -n1)"
	echo "LATEST_BIN: $LATEST_BIN"

	if rsync --rsh "$SSH_COMMAND" binaries@ftp.osmocom.org:web-files/osmo-opencm3-projects/all/ | grep -q "$LATEST_BIN"; then
		echo "Skipping upload, $LATEST_BIN has already been uploaded."
	else
		rsync --archive --copy-links --verbose --compress --delete --rsh "$SSH_COMMAND" \
			${proj}.{bin,elf,srec} \
			binaries@ftp.osmocom.org:web-files/osmo-opencm3-projects/latest/
		rsync --archive --verbose --compress --rsh "$SSH_COMMAND" \
			${proj}-*.{bin,elf,srec} \
			binaries@ftp.osmocom.org:web-files/osmo-opencm3-projects/all/
	fi

	echo "=============== $proj UPLOAD DONE =============="
done
