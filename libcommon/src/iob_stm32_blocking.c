#include <stdio.h>
#include <sys/cdefs.h>
#include <libopencm3/stm32/usart.h>

#include <libcommon/iob.h>

/* picolibc iob implementation for blocking I/O on USART */

static uint32_t stdio_usart;

void iob_init(uint32_t usart)
{
	stdio_usart = usart;
}

static int my_putc(char c, FILE *file)
{
	(void) file;
	usart_send_blocking(stdio_usart, c);
	return c;
}

static int my_getc(FILE *file)
{
	(void) file;
	return usart_recv_blocking(stdio_usart);
}

static int my_flush(FILE *file)
{
	(void) file;
	return 0;
}

static FILE __stdio = FDEV_SETUP_STREAM(my_putc, my_getc, my_flush, _FDEV_SETUP_RW);

FILE *const __iob[3] = { &__stdio, &__stdio, &__stdio };

#include <libcommon/microvty.h>

bool microvty_cb_uart_rx_not_empty(void)
{
	return usart_get_flag(stdio_usart, USART_SR_RXNE);
}
