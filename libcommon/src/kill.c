#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>

pid_t getpid(void) { return 1; }

int kill(pid_t pid, int sig) { if (pid == 1) _exit(sig << 8); errno = ESRCH; return -1; }
