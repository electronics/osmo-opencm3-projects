/*
 * Main firmware program for the sysmocom rfdsatt-4ch (RF digital step attenuator; 4 channel)
 *
 * Copyright (C) 2021 Harald Welte <laforge@osmocom.org>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/i2c.h>
#include <libopencm3/stm32/desig.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/cm3/systick.h>

#include <librfn/time.h>
#include <librfn/util.h>
#include <librfn/fibre.h>

#include <libcommon/iob.h>
#include <libcommon/utils.h>
#include <libcommon/microvty.h>

#include <stdio.h>
#include <stdlib.h>

#include "attenuator.h"
#include "eeprom.h"
#include "misc.h"

static uint32_t last_reset_cause;

extern const struct attenuator_cfg board_att_cfg;
extern struct attenuator_state *board_att_st[];

/* busy wait specified number of ms */
void msleep(uint32_t delay)
{
	uint32_t wake = time_now() + delay*1000;
	do {
	} while (wake > time_now());
}

/***********************************************************************
 * hardware initialization
 ***********************************************************************/

static void clock_setup(void)
{
	rcc_clock_setup_pll(&rcc_hsi_configs[RCC_CLOCK_HSI_48MHZ]);
	/* HCLK=48MHz; ADC=6MHz; APB1=24MHz; APB2=48MHz; USB=48MHz */

	/* tick rate of 1ms */

	/* Enable GPIOC clock. */
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_GPIOC);

	/* Enable clocks for USARTs */
	rcc_periph_clock_enable(RCC_USART1);
	rcc_periph_clock_enable(RCC_USART2);
}

static void i2c_setup(void)
{
	rcc_periph_clock_enable(RCC_I2C1);

#ifdef STM32F1
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN,
		      GPIO_I2C1_RE_SCL | GPIO_I2C1_RE_SDA);
#else
	gpio_set_af(GPIOB, GPIO_AF1, GPIO8 | GPIO9);
#endif

	/* disable before making config changes */
	i2c_peripheral_disable(I2C1);

#ifdef STM32F1
	/* APB1 runs at 24 MHz */
	i2c_set_clock_frequency(I2C1, 24);
#endif

	/* 400 kHz fast mode I2C */
	i2c_set_speed(I2C1, i2c_speed_fm_400k, 24);

	//...

	i2c_peripheral_enable(I2C1);
}

static void usart_setup(void)
{
	/* USART1: connected to debug header */

	/* Setup GPIO pin GPIO_USART1_TX. */
#ifdef STM32F1
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART1_TX);
#else
	gpio_set_af(GPIOA, GPIO_AF1, GPIO9 | GPIO10);
#endif
	/* Setup UART parameters. */
	usart_set_baudrate(USART1, 115200);
	usart_set_databits(USART1, 8);
	usart_set_stopbits(USART1, USART_STOPBITS_1);
	usart_set_mode(USART1, USART_MODE_TX_RX);
	usart_set_parity(USART1, USART_PARITY_NONE);
	usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);
	/* Finally enable the USART. */
	usart_enable(USART1);

	/* USART2: connected to 2.5mm stereo jack */

	/* Setup GPIO pin GPIO_USART2_TX. */
#ifdef STM32F1
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART2_TX);
#else
	gpio_set_af(GPIOA, GPIO_AF1, GPIO2 | GPIO13);
#endif
	/* Setup UART parameters. */
	usart_set_baudrate(USART2, 115200);
	usart_set_databits(USART2, 8);
	usart_set_stopbits(USART2, USART_STOPBITS_1);
	usart_set_mode(USART2, USART_MODE_TX_RX);
	usart_set_parity(USART2, USART_PARITY_NONE);
	usart_set_flow_control(USART2, USART_FLOWCONTROL_NONE);
	/* Finally enable the USART. */
	usart_enable(USART2);
}

static void gpio_setup(void)
{
#ifdef STM32F1
	/* disable JTAG, keep SWJ-DP (we use some related GPIOs) */
	rcc_periph_clock_enable(RCC_AFIO);
	gpio_primary_remap(AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON, AFIO_MAPR_PD01_REMAP | AFIO_MAPR_I2C1_REMAP);

	/* Set GPIO15 (in GPIO port B) to 'output push-pull' for the LED. */
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ,
		      GPIO_CNF_OUTPUT_PUSHPULL, GPIO15);
#else
	gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO15);
#endif
}

/***********************************************************************
 * attenuator eeprom store
 ***********************************************************************/

#include "board.h"

struct eeprom_chan {
	uint8_t stage_qdb[NUM_STAGE];
};

struct eeprom_state {
	uint8_t version;
	uint8_t num_chan:4,
		num_stage:4;
	struct eeprom_chan chan[NUM_CHAN];
} __attribute__ ((packed));

static int store_to_eeprom(void)
{
	struct eeprom_state est = {
		.version = 1,
		.num_chan = NUM_CHAN,
		.num_stage = NUM_STAGE,
	};
	const uint8_t *_est = (const uint8_t *) &est;
	unsigned int i;

	for (i = 0; i < NUM_CHAN; i++) {
		for (int s = 0; s < NUM_STAGE; s++)
			est.chan[i].stage_qdb[s] = attenuator_stage_get(i+1, s+1, ATT_VAL_CURRENT);
	}
	/* write to EEPROM */
	for (i = 0; i < sizeof(est); i++) {
		int rc = eeprom_write_byte(i, _est[i]);
		if (rc < 1) {
			printf("Error writing to EEPROM byte %u\r\n", i);
			return rc;
		}
	}

	printf("Saved current settings to EEPROM\r\n");
	return 0;
}

static int load_from_eeprom(void)
{
	struct eeprom_state est;
	unsigned int i;
	int rc;

	rc = eeprom_read((uint8_t *) &est, sizeof(est), 0);
	if (rc < 0) {
		printf("Failed to read from EEPROM: %d\r\n", rc);
		return rc;
	}
	if (est.version != 1) {
		if (est.version == 255)
			printf("EEPROM empty, cannot restore settings\r\n");
		else
			printf("Unknown EEPROM version: %d\r\n", est.version);
		return -1;
	}
	if (est.num_chan != NUM_CHAN || est.num_stage != NUM_STAGE) {
		printf("Unexpected EEPROM contents\r\n");
		return -1;
	}
	for (i = 0; i < est.num_chan; i++) {
		for (int s = 0; s < est.num_stage; s++)
			attenuator_stage_set(i+1, s+1, est.chan[i].stage_qdb[s]);
	}
	printf("EEPROM settings restored\r\n");
	return 0;
}


/***********************************************************************
 * uVTY interface
 ***********************************************************************/

DEFUN(reset, reset_cmd, "reset", "Reset the board")
{
	scb_reset_system();
}

DEFUN(uuid, uuid_cmd, "uuid", "Get the unique ID")
{
	char uuid[4*3*2+1];
	desig_get_unique_id_as_string(uuid, sizeof(uuid));
	printf("UUID: %s\r\n", uuid);
}

DEFUN(version, version_cmd, "version", "Get the firmware version")
{
	printf("Firmware Version: %s\r\n", GIT_VERSION);
}

DEFUN(att_show, att_show_cmd, "show", "Show state of all attenuators")
{
	unsigned int channel;

	for (channel = 1; channel <= board_att_cfg.num_channels; channel++) {
		unsigned int stage_idx;
		int stage_qdb[2];
		int sum_qdb = 0;

		for (stage_idx = 0; stage_idx < ARRAY_SIZE(stage_qdb); stage_idx++) {
			stage_qdb[stage_idx] = attenuator_stage_get(channel, stage_idx+1, ATT_VAL_CURRENT);
			sum_qdb += stage_qdb[stage_idx];
		}
		printf("Channel %02u: Stage1 %02d dB, Stage2 %02d dB, Sum %02d dB\r\n",
			channel, stage_qdb[0]/4, stage_qdb[1]/4, sum_qdb/4);
	}
}

DEFUN(att_set, att_set_cmd, "set",
	"Set an attenuator (channel, stage, dB)")
{
	int channel, stage, dB;
	int rc;

	if (argc < 3 || argc > 4) {
		printf("You must specify two (channel, dB) or three arguments (channel, stage, db)\r\n");
		return;
	}
	switch (argc) {
	case 3:
		channel = atoi(argv[1]);
		dB = atoi(argv[2]);
		rc = attenuator_chan_set(channel, dB*4, true);
		break;
	case 4:
		channel = atoi(argv[1]);
		stage = atoi(argv[2]);
		dB = atoi(argv[3]);
		rc = attenuator_stage_set(channel, stage, dB*4);
		break;
	default:
		printf("You must specify two (channel, dB) or three arguments (channel, stage, db)\r\n");
		break;
	}

	if (rc < 0)
		printf("Error setting attenuator %d: %d\r\n", channel, rc);
}

DEFUN(save, save_cmd, "save",
	"Save the current state to the EEPROM")
{
	store_to_eeprom();
}

DEFUN(load, load_cmd, "load",
	"Load all settings from the EEPROM")
{
	load_from_eeprom();
}



DEFUN(interact, interact_cmd, "interact", "Enter interactive single-key mode")
{
	uint8_t g_chan = 1;
	uint8_t g_stage = 1;
	uint8_t g_val = 0;

	printf("Entering interactive single-key mode. Press 'X' for exit, '?' for help\r\n");

	while (1) {
		int ch, rc;
		/* read one blocking char */
		do {
			ch = getchar();
		} while (ch == EOF);

		switch (ch) {
		case '?':
			printf(	"'1'..'4': Select channel\r\n"
				"'0'     : Set attenuation of current channel+stage to 0 dB\r\n"
				"'f'     : Set attenuation of current channel+stage to 31 dB\r\n"
				"'+'/'-' : Increment/decrement attenuation in dB\r\n"
				"'a'/'b' : Select stage 1/2 within channel\r\n"
				"'X'     : Exit interactive mode\r\n"
				);
			break;
		case 'X': /* exit */
			return;
		case '1': case '2': case '3': case '4': /* channel number */
			g_chan = ch - '0';
			g_stage = 1;
			g_val = attenuator_stage_get(g_chan, g_stage, ATT_VAL_CURRENT);
			break;
		case '0': /* set to 0dB */
			g_val = 0;
			break;
		case 'f': /* set to full attenuation */
			g_val = 31;
			break;
		case '+': /* increment by 1dB */
			if (g_val < 31)
				g_val++;
			break;
		case '-': /* decrement by 1dB */
			if (g_val > 0)
				g_val--;
			break;
		case 'a': /* stage 1 */
			if (g_stage != 1) {
				g_stage = 1;
				g_val = attenuator_stage_get(g_chan, g_stage, ATT_VAL_CURRENT);
			}
			break;
		case 'b': /* stage 2 */
			if (g_stage != 2) {
				g_stage = 2;
				g_val = attenuator_stage_get(g_chan, g_stage, ATT_VAL_CURRENT);
			}
			break;
		}
		rc = attenuator_stage_set(g_chan, g_stage, g_val*4);
		if (rc < 0)
			printf("Error setting attenuator: %d\r\n", rc);
	}
}

DEFUN(test, test_cmd, "test", "Enter interactive test-ramp mode")
{
	uint8_t g_chan = 1;
	uint8_t g_val = 0;

	printf("Interactive test-ramp mode. Press 'X' for exit, '?' for help\r\n");

	while (1) {
		int ch, rc;
		if ((ch = getchar()) != EOF) {
			switch (ch) {
			case '?':
				printf(	"'1'..'4': Select channel\r\n"
					"'0'     : Set attenuation of current channel to 0 dB\r\n"
					"'a'/'b' : Select stage 1/2 within channel\r\n"
					"'X'     : Exit interactive test-ramp mode\r\n"
					);
				break;
			case 'X': /* exit */
				return;
			case '1': case '2': case '3': case '4': /* channel number */
				g_chan = ch - '0';
				g_val = 0;
				break;
			case '0': /* set to 0dB */
				g_val = 0;
				break;
			}
		}
		if (g_val < 60)
			g_val += 10;
		else
			g_val = 0;

		rc = attenuator_chan_set(g_chan, g_val*4, false);
		if (rc < 0)
			printf("Error setting attenuator: %d\r\n", rc);

		msleep(500);
	}
}


DEFUN(i2c_read, i2c_read_cmd, "i2c-read", "Read from I2C EEPROM")
{
	uint16_t mem_addr = 0;
	uint8_t rbuf[16];
	unsigned int i;

	i = eeprom_read(rbuf, sizeof(rbuf), mem_addr);
	printf("Read result: %d\r\n", i);
	for (i = 0; i < sizeof(rbuf); i++)
		printf("%02x ", rbuf[i]);
	printf("\r\n");
}

DEFUN(i2c_write, i2c_write_cmd, "i2c-write", "write to I2C EEPROM")
{
	int rc, addr, val;

	if (argc < 3) {
		printf("you have to specify address + value\r\n");
		return;
	}

	addr = atoi(argv[1]);
	val = atoi(argv[2]);

	printf("Writing 0x%02x to address 0x%02x\r\n", val, addr);
	rc = eeprom_write_byte(addr, val);
	printf("Write result: %d\r\n", rc);
}

/***********************************************************************
 * main
 ***********************************************************************/

static void print_banner(void)
{
	char uuid[4*3*2+1];
	desig_get_unique_id_as_string(uuid, sizeof(uuid));

	printf("\r\n======================================================================\r\n");
	printf("sysmocom RFDSATT main (build %s)\r\n", GIT_VERSION);
	printf("UUID: %s\r\n", uuid);
	printf("Reset cause(s): 0x%08lx\r\n", last_reset_cause);
	printf("======================================================================\r\n\r\n");
}

typedef struct {
	uint32_t time;
	fibre_t fibre;
} led_fibre_t;

int led_fibre(fibre_t *fibre)
{
	led_fibre_t *led = containerof(fibre, led_fibre_t, fibre);

	PT_BEGIN_FIBRE(fibre);

	led->time = time_now();

	while (true) {
		led->time += 500000;
		PT_WAIT_UNTIL(fibre_timeout(led->time));
		gpio_toggle(GPIOB, GPIO15);	/* LED on/off */
	}

	PT_END();
}

led_fibre_t led = {
	.fibre = FIBRE_VAR_INIT(led_fibre)
};

int main(void)
{
	/* get, store and clear the cause of the last reset */
	last_reset_cause = RCC_CSR & RCC_CSR_RESET_FLAGS;
	RCC_CSR |= RCC_CSR_RMVF;

	clock_setup();
	gpio_setup();
	usart_setup();
	iob_init(USART2);
	i2c_setup();
	time_init();
	attenuator_init(&board_att_cfg, board_att_st);

	print_banner();

	load_from_eeprom();

	microvty_init("rfdsat4ch> ");
	microvty_register(&reset_cmd);
	microvty_register(&uuid_cmd);
	microvty_register(&version_cmd);
	microvty_register(&att_show_cmd);
	microvty_register(&att_set_cmd);
	microvty_register(&interact_cmd);
	microvty_register(&test_cmd);
	microvty_register(&i2c_read_cmd);
	microvty_register(&i2c_write_cmd);
	microvty_register(&load_cmd);
	microvty_register(&save_cmd);
	microvty_print_prompt();

	fibre_run(&led.fibre);
	fibre_run(&microvty_fibre);

	fibre_scheduler_main_loop();

	return 0;
}
